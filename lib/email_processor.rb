class EmailProcessor
  def self.process(email)
    email.process
  end

  def process
    logger.debug email.from
  end
end
