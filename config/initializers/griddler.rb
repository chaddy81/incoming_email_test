Griddler.configure do |config|
  config.processor_class = EmailProcessor # MyEmailProcessor
  config.email_service = :mandrill # :cloudmailin, :postmark, :mandrill, :mailgun
end